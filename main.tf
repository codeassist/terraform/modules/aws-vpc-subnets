locals {
  enabled = lower(var.subnets_module_enabled) ? true : false

  tags = merge(
    var.subnets_tags,
    {
      terraform = true,
    },
  )

  # calculate amount of public/private subnets need to be created
  public_subnets_amount  = (var.subnets_az_amount * var.subnets_public_per_az)
  private_subnets_amount = (var.subnets_az_amount * var.subnets_private_per_az)

  # calculate amount of NAT resources
  nat_resources_amount = var.subnets_nat_resource_create ? (var.subnets_nat_resource_per_az_enabled ? var.subnets_az_amount : 1) : 0
  nat_gateways_amount  = lower(var.subnets_nat_resource_type) == "gateway" ? local.nat_resources_amount : 0
  nat_instances_amount = lower(var.subnets_nat_resource_type) == "instance" ? local.nat_resources_amount : 0

  # maximum number of subnets which can be created. This constant is being used for CIDR blocks calculation.
  max_total_subnets = var.subnets_per_az_max_count > 0 ? (var.subnets_az_amount * var.subnets_per_az_max_count) : ((local.public_subnets_amount + local.private_subnets_amount > 16) ? (local.public_subnets_amount + local.private_subnets_amount) : 16)
}


# Get `aws_vpc` object parameters by provide VPC ID value.
data "aws_vpc" "this" {
  # Provides details about a specific VPC.

  # This resource can prove useful when a module accepts a vpc id as an input variable and needs to, for example,
  # determine the CIDR block of that VPC.
  count = (local.enabled && length(var.subnets_vpc_cidr_block) == 0) ? 1 : 0

  # (Optional) The id of the specific VPC to retrieve.
  id = var.subnets_vpc_id
}
locals {
  vpc_cidr_block = length(var.subnets_vpc_cidr_block) > 0 ? var.subnets_vpc_cidr_block : join("", data.aws_vpc.this.*.cidr_block)
}

# Declare the data source to use amount of available AZs per the region in `length()` calculations
data "aws_availability_zones" "available" {
  # The Availability Zones data source allows access to the list of AWS Availability Zones which can be accessed
  # by an AWS account within the region configured in the provider.
  # This is different from the `aws_availability_zone` (singular) data source, which provides some details about
  # a specific availability zone.
  count = local.enabled ? 1 : 0

  # (Optional) Allows to filter list of Availability Zones based on their current state. Can be either:
  #   * available
  #   * information
  #   * impaired
  #   * unavailable.
  # By default the list includes a complete set of Availability Zones to which the underlying AWS account has access,
  # regardless of their state.
  state = "available"
}
