resource "aws_eip" "nat_gateway" {
  # Provides an Elastic IP resource.

  # Note(1): EIP may require IGW to exist prior to association. Use depends_on to set an explicit dependency on the IGW.

  # Note(2): Do not use `network_interface` to associate the EIP to `aws_lb` or `aws_nat_gateway` resources. Instead use
  # the `allocation_id` available in those resources to allow AWS to manage the association, otherwise you will see
  # `AuthFailure` errors.
  count = local.enabled ? local.nat_gateways_amount : 0

  # See:
  #   * https://discuss.hashicorp.com/t/tips-howto-implement-module-depends-on-emulation/2305
  depends_on = [
    var.subnets_module_depends_on
  ]

  # (Optional) Boolean if the EIP is in a VPC or not.
  vpc = true

  # (Optional) A mapping of tags to assign to the resource.
  # Override the "Name" tag from `local.vpc_tags`.
  tags = merge(
    local.tags,
    {
      AvailabilityZone = data.aws_availability_zones.available[0].names[count.index],
      Name             = format("%s-%s-nat-gateway-eip", var.subnets_vpc_name, data.aws_availability_zones.available[0].names[count.index]),
    }
  )

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_nat_gateway" "nat_gateway" {
  # Provides a resource to create a VPC NAT Gateway.
  count = local.enabled ? local.nat_gateways_amount : 0

  # Note: It's recommended to denote that the NAT Gateway depends on the Internet Gateway for the VPC in which
  # the NAT Gateway's subnet is located.
  # See:
  #   * https://discuss.hashicorp.com/t/tips-howto-implement-module-depends-on-emulation/2305
  depends_on = [
    var.subnets_module_depends_on
  ]

  # (Required) The Allocation ID of the Elastic IP address for the gateway.
  allocation_id = aws_eip.nat_gateway[count.index].id

  # (Required) The Subnet ID of the subnet in which to place the gateway.
  subnet_id = matchkeys(
    aws_subnet.public.*.id,
    aws_subnet.public.*.tags.AvailabilityZone,
    [data.aws_availability_zones.available[0].names[count.index]]
  )[0]

  # (Optional) A mapping of tags to assign to the resource.
  # Override the "Name" tag from `local.vpc_tags`.
  tags = merge(
    local.tags,
    {
      AvailabilityZone = data.aws_availability_zones.available[0].names[count.index],
      Name             = format("%s-%s-nat-gateway", var.subnets_vpc_name, data.aws_availability_zones.available[0].names[count.index]),
    }
  )

  lifecycle {
    create_before_destroy = true
  }
}

# if NAT gateway enabled, every route from Private subnets must points to either NAT gateway in the same AZ (in case
# of High Availability config) or the single NAT gateway created in one of available AZs.
resource "aws_route" "private_to_nat_gateway" {
  # Provides a resource to create a routing table entry (a route) in a VPC routing table.

  # NOTE on Route Tables and Routes:
  # Terraform currently provides both a standalone Route resource and a Route Table resource with routes defined in-line.
  # At this time you cannot use a Route Table with in-line routes in conjunction with any Route resources. Doing so
  # will cause a conflict of rule settings and will overwrite rules.
  count = (local.enabled && local.nat_gateways_amount > 0) ? var.subnets_az_amount : 0

  # (Required) The ID of the routing table.
  route_table_id = aws_route_table.private[count.index].id
  # (Optional) The destination CIDR block.
  destination_cidr_block = "0.0.0.0/0"

  # (Optional) Identifier of a VPC NAT gateway.
  # Select NAT Gateway either those located in the same AZ (in case of HA config), or the single one.
  nat_gateway_id = local.nat_gateways_amount > 1 ? matchkeys(aws_nat_gateway.nat_gateway.*.id, aws_nat_gateway.nat_gateway.*.tags.AvailabilityZone, [aws_route_table.private[count.index].tags.AvailabilityZone])[0] : aws_nat_gateway.nat_gateway[0].id
}
