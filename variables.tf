variable "subnets_module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default = false
}
variable "subnets_module_depends_on" {
  description = "Emulation of `depends_on` behavior for the module."
  type        = any
  # Non zero length string can be used to have current module wait for the specified resource.
  default = null
}

variable "subnets_tags" {
  description = "Additional tags to assign to the subnets and other resources."
  type        = map(string)
  default     = {}
}

variable "subnets_vpc_id" {
  description = "(Required) VPC ID where subnets will be created."
  type        = string
}

variable "subnets_vpc_name" {
  description = "(Required) Name associated with the VPC to distinct from others."
  type        = string
}

variable "subnets_vpc_cidr_block" {
  description = "The CIDR block of the VPC."
  # if not specified - will try to get from VPC parameters.
  type    = string
  default = ""
}

variable "subnets_igw_enabled" {
  description = "Whether to use the specified Internet Gateway ID to configure routes for Public subnets."
  type        = bool
  default     = false
}

variable "subnets_igw_id" {
  description = "Internet Gateway ID the public route table will point to."
  type        = string
  default     = ""
}

variable "subnets_az_amount" {
  description = "Amount of availability zones to create public/private subnets within the VPC."
  type        = number
  default     = 0
}

variable "subnets_per_az_max_count" {
  description = "To calculate CIDR block for every Subnet - override the maximum amount of subnets will possibly be deployed in every AZ."
  type        = number
  default     = 0
}

variable "subnets_public_per_az" {
  description = "How many public subnets in every AZ will be created."
  type        = number
  default     = 0
}

variable "subnets_private_per_az" {
  description = "How many private subnets in every AZ will be created."
  type        = number
  default     = 0
}

variable "subnets_nat_resource_create" {
  description = "Boolean switch to enable/disable NAT resources."
  type        = bool
  default     = false
}
variable "subnets_nat_resource_per_az_enabled" {
  description = "Enable/disable NAT resource creation in every AZ for HA scenario."
  type        = bool
  default     = false
}
variable "subnets_nat_resource_type" {
  description = "What kind of NAT resource to create to allow instances in private subnets to access the Internet."
  # The only possible values are (case insensitive):
  #   * instance (default)
  #   * gateway
  type    = string
  default = "instance"
}
variable "subnets_nat_instance_type" {
  description = "The type of NAT EC2 Instance."
  type        = string
  default     = "t3a.micro"
}
variable "subnets_nat_instance_static_eip" {
  description = "Whether to allocate EIP to the NAT instance."
  type        = bool
  default     = false
}
