output "public_subnets_ids" {
  description = "The list of the Public Subnet IDs."
  value       = aws_subnet.public.*.id
}
output "public_subnets_cidrs" {
  description = "CIDR blocks of the created public subnets."
  value       = aws_subnet.public.*.cidr_block
}
output "public_route_tables_ids" {
  description = "IDs of the created public route tables."
  value       = aws_route_table.public.*.id
}
output "public_subnets_id_az_map" {
  description = "Map of the Availability Zones to the Public subnet IDs."
  # Creates a map from a list of keys and a list of values. The keys must all be of type string, and the length
  # of the lists must be the same.
  value = zipmap(aws_subnet.public.*.id, aws_subnet.public.*.tags.AvailabilityZone)
}
output "public_subnets_id_name_map" {
  description = "Map of the Public Subnet Names to their IDs."
  # Creates a map from a list of keys and a list of values. The keys must all be of type string, and the length
  # of the lists must be the same.
  value = zipmap(aws_subnet.public.*.id, aws_subnet.public.*.tags.Name)
}

output "private_subnets_ids" {
  description = "The list of the Private Subnet IDs."
  value       = aws_subnet.private.*.id
}
output "private_subnets_cidrs" {
  description = "CIDR blocks of the created private subnets."
  value       = aws_subnet.private.*.cidr_block
}
output "private_route_tables_ids" {
  description = "IDs of the created private route tables."
  value       = aws_route_table.private.*.id
}
output "private_subnets_id_az_map" {
  description = "Map of the Availability Zones to the Private subnet IDs."
  # Creates a map from a list of keys and a list of values. The keys must all be of type string, and the length
  # of the lists must be the same.
  value = zipmap(aws_subnet.private.*.id, aws_subnet.private.*.tags.AvailabilityZone)
}
output "private_subnets_id_name_map" {
  description = "Map of the Private Subnet Names to their IDs."
  # Creates a map from a list of keys and a list of values. The keys must all be of type string, and the length
  # of the lists must be the same.
  value = zipmap(aws_subnet.private.*.id, aws_subnet.private.*.tags.Name)
}

output "nat_gateways_ids" {
  description = "The list of NAT Gateways IDs."
  value       = aws_nat_gateway.nat_gateway.*.id
}
output "nat_gateways_eip_ids" {
  description = "The list of Elastic IP IDs attached to the NAT Gateway(s)."
  value       = aws_eip.nat_gateway.*.id
}
output "nat_gateways_id_ip_map" {
  description = "The map of public IP addresses allocated for the NAT Gateway(s)."
  value       = zipmap(aws_nat_gateway.nat_gateway.*.id, aws_nat_gateway.nat_gateway.*.public_ip)
}
output "nat_gateways_id_az_map" {
  description = "The map of Availability Zones the NAT Gateway(s) resides in."
  value       = zipmap(aws_nat_gateway.nat_gateway.*.id, aws_nat_gateway.nat_gateway.*.tags.AvailabilityZone)
}

output "nat_instances_ids" {
  description = "The list of NAT Instances IDs."
  value       = aws_instance.nat_instance.*.id
}
output "nat_instances_eip_ids" {
  description = "The list of Elastic IP IDs attached to the NAT Instance(s)."
  value       = aws_eip.nat_instance.*.id
}
output "nat_instances_id_ip_map" {
  description = "The map of public IP addresses allocated for the NAT Instance(s)."
  value       = var.subnets_nat_instance_static_eip ? zipmap(aws_eip.nat_instance.*.instance, aws_eip.nat_instance.*.public_ip) : zipmap(aws_instance.nat_instance.*.id, aws_instance.nat_instance.*.public_ip)
}
output "nat_instances_id_az_map" {
  description = "The map of Availability Zones the NAT Instance(s) resides in."
  value       = zipmap(aws_instance.nat_instance.*.id, aws_instance.nat_instance.*.tags.AvailabilityZone)
}
