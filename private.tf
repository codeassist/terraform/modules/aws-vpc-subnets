# -------------------------------
# Manage VPC's "Private" Subnets
# -------------------------------
resource "aws_subnet" "private" {
  # Provides an VPC subnet resource.
  count = local.enabled ? local.private_subnets_amount : 0

  # (Required) The VPC ID.
  vpc_id = var.subnets_vpc_id
  # (Optional) The AZ for the subnet.
  availability_zone = data.aws_availability_zones.available[0].names[floor(count.index / var.subnets_private_per_az)]
  # (Required) The CIDR block for the subnet.
  cidr_block = cidrsubnet(
    local.vpc_cidr_block,
    ceil(log(local.max_total_subnets, 2)),
    local.max_total_subnets - count.index - 1,
  )

  # (Optional) Specify true to indicate that instances launched into the subnet should be assigned a public IP address.
  map_public_ip_on_launch = false

  # (Optional) A mapping of tags to assign to the resource.
  # Override the "Name" tag from `local.vpc_tags` and assign some more specific for the current resource.
  tags = merge(
    local.tags,
    {
      SubnetType       = "private"
      AvailabilityZone = data.aws_availability_zones.available[0].names[floor(count.index / var.subnets_private_per_az)]
      Name = format(
        "%s-%s-private-subnet",
        var.subnets_vpc_name,
        data.aws_availability_zones.available[0].names[floor(count.index / var.subnets_private_per_az)]
      )
    },
  )

  lifecycle {
    # Ignore tags added by kops or kubernetes
    ignore_changes = [
      tags.kubernetes,
      tags.SubnetType,
    ]
  }
}

# Evaluation amount of private route table differs from the same public resource definition, because instead of the
# single igw in every AZ we have the separate natgw configured:
resource "aws_route_table" "private" {
  # Provides a resource to create a VPC routing table.

  # NOTE(1) on Route Tables and Routes:
  # Terraform currently provides both a standalone Route resource and a Route Table resource with routes defined in-line.
  # At this time you cannot use a Route Table with in-line routes in conjunction with any Route resources. Doing so
  # will cause a conflict of rule settings and will overwrite rules.

  # NOTE(2) on `gateway_id` and `nat_gateway_id`:
  # The AWS API is very forgiving with these two attributes and the `aws_route_table` resource can be created
  # with a NAT ID specified as a Gateway ID attribute. This will lead to a permanent diff between your configuration
  # and statefile, as the API returns the correct parameters in the returned route table. If you're experiencing
  # constant diffs in your `aws_route_table` resources, the first thing to check is whether or not you're specifying
  # a NAT ID instead of a Gateway ID, or vice-versa.

  # NOTE(3) on `propagating_vgws` and the `aws_vpn_gateway_route_propagation` resource:
  # If the `propagating_vgws` argument is present, it's not supported to also define route propagations using
  # `aws_vpn_gateway_route_propagation`, since this resource will delete any propagating gateways not explicitly
  # listed in `propagating_vgws`. Omit this argument when defining route propagation using the separate resource.
  count = (local.enabled && local.private_subnets_amount > 0) ? var.subnets_az_amount : 0

  # (Required) The VPC ID.
  vpc_id = var.subnets_vpc_id

  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      AvailabilityZone = data.aws_availability_zones.available[0].names[count.index],
      Name             = format("%s-%s-private-rt", var.subnets_vpc_name, data.aws_availability_zones.available[0].names[count.index]),
    }
  )
}

# Match every subnet with route table in the same AZ.
resource "aws_route_table_association" "private" {
  # Provides a resource to create an association between a subnet and routing table.
  count = local.enabled ? local.private_subnets_amount : 0

  # (Required) The subnet ID to create an association.
  subnet_id = aws_subnet.private[count.index].id
  # (Required) The ID of the routing table to associate with.
  route_table_id = matchkeys(
    aws_route_table.private.*.id,
    aws_route_table.private.*.tags.AvailabilityZone,
    [data.aws_availability_zones.available[0].names[floor(count.index / var.subnets_private_per_az)]]
  )[0]
}

resource "aws_network_acl" "private" {
  # Provides an network ACL resource. You might set up network ACLs with rules similar to your security groups in order
  # to add an additional layer of security to your VPC.

  # NOTE on Network ACLs and Network ACL Rules:
  # Terraform currently provides both a standalone Network ACL Rule resource and a Network ACL resource with rules
  # defined in-line. At this time you cannot use a Network ACL with in-line rules in conjunction with
  # any Network ACL Rule resources. Doing so will cause a conflict of rule settings and will overwrite rules.
  count = (local.enabled && local.private_subnets_amount > 0) ? 1 : 0

  # (Required) The ID of the associated VPC.
  vpc_id = var.subnets_vpc_id
  # (Optional) A list of Subnet IDs to apply the ACL to
  subnet_ids = aws_subnet.private.*.id

  # (Optional) Specifies an egress rule. Parameters defined below. This argument is processed in
  # attribute-as-blocks (https://www.terraform.io/docs/configuration/attr-as-blocks.html) mode.
  egress {
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
  }

  # (Optional) Specifies an ingress rule. Parameters defined below. This argument is processed in
  # attribute-as-blocks (https://www.terraform.io/docs/configuration/attr-as-blocks.html) mode.
  ingress {
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
  }

  # (Optional) A mapping of tags to assign to the resource.
  # Override the "Name" tag from `local.vpc_tags`.
  tags = merge(
    local.tags,
    {
      Name = format("%s-private-acl", var.subnets_vpc_name),
    }
  )
}
