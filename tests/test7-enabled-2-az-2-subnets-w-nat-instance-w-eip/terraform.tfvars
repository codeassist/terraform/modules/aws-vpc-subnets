# Whether to create the resources (`false` prevents the module from creating any resources).
subnets_module_enabled    = true
//# Emulation of `depends_on` behavior for the module.
//# Non zero length string can be used to have current module wait for the specified resource.
//subnets_module_depends_on = ""
# Additional tags to assign to the subnets and other resources.
subnets_tags              = {
  tag1 = "some value"
}
# (Required) VPC ID where subnets will be created.
subnets_vpc_id            = "vpc-test7"
# (Required) Name associated with the VPC to distinct from others.
subnets_vpc_name          = "test7-vpc"
# The CIDR block of the VPC.
subnets_vpc_cidr_block    = "172.16.0.0/22"
# Whether to create the Internet Gateway in a VPC to allow public access for resources reside in.
subnets_igw_enabled       = true
# Internet Gateway ID the public route table will point to.
subnets_igw_id            = "igw-test7"
# Amount of availability zones to create public/private subnets within the VPC.
subnets_az_amount         = 2
//# To calculate CIDR block for every Subnet - override the maximum amount of subnets will possibly be deployed in every AZ.
//subnets_per_az_max_count         = 0
# How many public subnets in every AZ must be created.
subnets_public_per_az  = 2
# How many private subnets in every AZ must be created.
subnets_private_per_az = 2
# Boolean switch to enable/disable NAT resources.
subnets_nat_resource_create   = true
# Enable/disable NAT resource creation in every AZ for HA scenario.
subnets_nat_resource_per_az_enabled = false
# What kind of NAT resource to create to allow instances in private subnets to access the Internet.
# The only possible values are (case insensitive):
#   * instance (default)
#   * gateway
subnets_nat_resource_type = "instance"
//# The type of NAT EC2 Instance.
//subnets_nat_instance_type = "t3a.micro"
# Whether to allocate EIP to the NAT instance.
subnets_nat_instance_static_eip = false
